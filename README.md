# Gitlab Deploy Key

This is primarily a CLI tool that helps you set a deploy key for a gitlab repo.

The point was that I needed to run this on a Gitlab instance that was private/internal by default, so that CI/CD pipelines could clone Ansible roles.

## Install

    $ pipenv install

## Configure

Create a file in ~/.gl-deploy-key.cfg with the following contents.

    [gl-deploy-key]
    token=secret.access.token
    url=your.base.gitlab.url/api/v4/

## Usage

    $ pipenv run python -m gllib.script --help
    $ pipenv run python -m gllib.script listProjects

## Gitlab behind socks proxy

To avoid affecting other applications in your shell you can add environment variables to an .env file.

    HTTP_PROXY=socks5://localhost:8080
    HTTPS_PROXY=socks5://localhost:8080

The script will notify you when the .env file is used.

    $ pipenv run python -m gllib.script listProjects
    Loading .env environment variables…
    ...

