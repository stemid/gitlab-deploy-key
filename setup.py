from distutils.core import setup

setup(
    name = 'gitlab-deploy-key',
    packages = ['gllib'],
    version = '0.1',
    license='MIT',
    description = 'Set deploy keys for Gitlab repos with the API.',
    author = 'Stefan Midjich',
    author_email = 'swehack@gmail.com',
    url = 'https://gitlab.com/stemid/gitlab-deploy-key.git',
    #download_url = 'https://github.com/joelbarmettlerUZH/Scrapeasy/archive/pypi-0_1_3.tar.gz',
    keywords = ['gitlab', 'cli'],
    install_requires=[
        'requests[socks]',
    ],
    entry_points = {
        'console_scripts': [
            'hl-deploy-key=gllib.command:run_command'
        ]
    },
    classifiers=[  # Optional
        # How mature is this project? Common values are
        #   3 - Alpha
        #   4 - Beta
        #   5 - Production/Stable
        'Development Status :: 3 - Alpha',

        # Indicate who your project is intended for
        'Intended Audience :: Developers',
        'Topic :: Software Development :: Build Tools',

        # Pick your license as you wish
        'License :: OSI Approved :: MIT License',

        # Specify the Python versions you support here. In particular, ensure
        # that you indicate whether you support Python 2, Python 3 or both.
        'Programming Language :: Python :: 3',
        'Programming Language :: Python :: 3.4',
        'Programming Language :: Python :: 3.5',
        'Programming Language :: Python :: 3.6',
        'Programming Language :: Python :: 3.7',
    ],
)
