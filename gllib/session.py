from urllib.parse import urljoin
from requests import Session

from gllib import GLLIB_DEFAULT_PROJECTS_PER_PAGE


class GitlabSession(Session):

    def __init__(self, **kw):
        token = kw.pop('token')
        baseUrl = kw.pop('base_url', 'https://gitlab.com/api/v4')
        self.base_url = baseUrl

        super(GitlabSession, self).__init__()
        self.headers.update({
            'PRIVATE-TOKEN': token,
        })


    def request(self, method, url, *args, **kwargs):
        """Send the request after generating the complete URL."""
        url = self.create_url(url)
        return super(GitlabSession, self).request(
            method, url, *args, **kwargs
        )


    def create_url(self, url):
        """Create the URL based off this partial path."""
        return urljoin(self.base_url, url)


    def search_projects(self, searchString=None, params={}):
        params = {}
        params = {
            **params,
            'order_by': 'id',
            'sort': 'desc',
            'per_page': GLLIB_DEFAULT_PROJECTS_PER_PAGE,
        }
        if searchString:
            params['search'] = searchString

        r = self.get(
            'projects',
            params=params
        )
        return r.json()
