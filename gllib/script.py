#from pprint import pprint as pp
from sys import stderr, exit
from os.path import expanduser
from argparse import ArgumentParser, FileType
from configparser import RawConfigParser
from fnmatch import fnmatch

try:
    from dotenv import load_dotenv
    load_dotenv(verbose=False)
except ImportError:
    pass

from gllib import GLLIB_DEFAULT_CONFIG_FILE
from gllib import GLLIB_APP_NAME as APP_NAME
from gllib.session import GitlabSession


def parse_arguments():
    parser = ArgumentParser(
        description='Manage Gitlab deploy keys with API'
    )

    parser.add_argument(
        '--config', '-C',
        type=FileType('r'),
        default=open(expanduser(GLLIB_DEFAULT_CONFIG_FILE)),
        required=False,
        help='Specify another config file (default: {filename})'.format(
            filename=GLLIB_DEFAULT_CONFIG_FILE
        )
    )

    parser.add_argument(
        '--url', '-U',
        action='store',
        help=(
            'Specify a Gitlab instance API URL (example: '
            'https://gitlab.com/api/v4)'
        )
    )

    subparsers = parser.add_subparsers(
        title='Sub-commands',
        description='Valid sub-commands',
        dest='subcommand',
        help='sub-command help'
    )

    parser_listProjects = subparsers.add_parser(
        'listProjects',
        help='List accessible projects with your access token'
    )

    parser_listProjects.add_argument(
        '--search',
        default=None,
        help='Search for projects matching this name'
    )

    parser_listKeys = subparsers.add_parser(
        'listKeys',
        help='List deploy keys'
    )

    parser_listKeys.add_argument(
        '--search',
        help='Search for deploy key titles matching this string'
    )

    parser_enableKey = subparsers.add_parser(
        'enableKey',
        description='Must specify one of --project or --search arguments',
        help='Enable one deploy key for several projects'
    )

    parser_enableKey.add_argument(
        '--project',
        nargs='?',
        metavar='PROJECT_ID',
        help='Specify project IDs (multiple times)'
    )

    parser_enableKey.add_argument(
        '--search',
        help='Specify projects to enable key for by search pattern'
    )

    parser_enableKey.add_argument(
        'key',
        type=int,
        metavar='KEY_ID',
        help='Deploy key ID to enable'
    )

    parser_disableKey = subparsers.add_parser(
        'disableKey',
        description='Must specify one of --project or --search arguments',
        help='Disable deploy key for one or more projects'
    )

    parser_disableKey.add_argument(
        '--project',
        nargs='?',
        metavar='PROJECT_ID',
        help='Specify project IDs'
    )

    parser_disableKey.add_argument(
        '--search',
        help='Specify projects to disable key for by search pattern'
    )

    parser_disableKey.add_argument(
        'key',
        type=int,
        metavar='KEY_ID',
        help='Deploy key ID to disable'
    )

    return parser.parse_args()


def main():
    try:
        args = parse_arguments()
    except FileNotFoundError as e:
        print(
            (
                'You need to set your Gitlab API token in '
                '{filename}, or use --config file.cfg'
            ).format(
                filename=GLLIB_DEFAULT_CONFIG_FILE
            ),
            file=stderr
        )
        return 1

    config = RawConfigParser()
    config.read_file(args.config)

    if args.url:
        baseUrl = args.url
    else:
        baseUrl = config.get(APP_NAME, 'url')

    gl = GitlabSession(
        base_url=baseUrl,
        token=config.get(APP_NAME, 'token')
    )
    gl.proxies = {}

    if config.has_option(APP_NAME, 'socks_proxy'):
        gl.proxies.update({
            'http': config.get(APP_NAME, 'socks_proxy'),
            'https': config.get(APP_NAME, 'socks_proxy')
        })

    if args.subcommand == 'listProjects':
        for project in gl.search_projects(args.search):
            print('{projectId}: {projectName}'.format(
                projectId=project.get('id'),
                projectName=project.get('name')
            ))

    if args.subcommand == 'listKeys':
        matchedKeys = []

        r = gl.get('deploy_keys')

        for key in r.json():
            if args.search:
                if fnmatch(key.get('title'), args.search):
                    matchedKeys.append(key)
                continue
            matchedKeys.append(key)

        for key in matchedKeys:
            print('{keyId}: {keyTitle}'.format(
                keyId=key.get('id'),
                keyTitle=key.get('title')
            ))

    if args.subcommand == 'enableKey':
        # First gather a list of projects to enable for
        projects = []
        if isinstance(args.project, list):
            projects = args.project
        elif args.search:
            projects = gl.search_projects(args.search)
        else:
            print(
                'Must specify one of --project or --search arguments',
                file=stderr
            )
            return 1


if __name__ == '__main__':
    exit(main())
